import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/models/player';
import { PlayerService } from 'src/app/services/player.service';
import { DialogService } from '../../common/services/dialog.service';
import { DialogComponent } from '../../common/components/dialog/dialog.component';
import { DIALOG_TYPES } from '../../common/constants/dialog-types';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  players: Player[];
  player: Player;
  btnVal: string;

  constructor(private playerService: PlayerService, private dialogService: DialogService) {
    this.player = new Player();
    this.btnVal = "Add";
  }

  addPlayer(playerForm: any): void {
    let cPlayer = playerForm.form.value;
    this.removeExistingPlayer(cPlayer);
    this.players = this.playerService.populate(cPlayer.name, cPlayer.age);
    this.btnVal = "Add";
    playerForm.resetForm();
  }

  editPlayer(idx: number) {
    const ePlayer = this.players[idx];
    this.player.age = ePlayer.age;
    this.player.name = ePlayer.name;
    this.player.idx = idx;
    this.btnVal = "Update";
  }

  private removeExistingPlayer(player: Player): void {
    if (this.btnVal = "Update") {
      if (player.idx != undefined) {
        let ePlayer = this.players[player.idx];
        if (ePlayer) {
          this.playerService.delete(player.idx);
        }
      }
    }
  }

  deletePlayer(idx: number): void {
    const data = {
      type: DIALOG_TYPES.CONFIRM,
      title: "Confirm Delete",
      content: "Are you sure you want to delete the player ?"
    };
    const dialog = this.dialogService.openDialog(data, DialogComponent);
    dialog.afterClosed().subscribe((response: any) => {
      if (true == response) {
        this.playerService.delete(idx);
      }
    });
  }

  ngOnInit() {
  }

}
