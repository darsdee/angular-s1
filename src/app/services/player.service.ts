import { Injectable } from '@angular/core';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  players: Player[];

  constructor() {
    this.players = [];
  }

  populate(name: string, age: number): Player[] {
    this.add(name, age);
    return this.players;
  }

  private add(name: string, age: number): void {
    let player = new Player();
    player.name = name;
    player.age = age;
    this.players.push(player);
  }

  delete(idx: number) {
    this.players.splice(idx, 1);
  }

  private sort(): void {
    this.players.sort((p1, p2) => p1.name.localeCompare(p2.name));
  }
}
