import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { AppComponent } from './app.component';
import { PlayerComponent } from './components/player/player.component';
import { NumberRequiredDirective } from './common/validators/number-required.directive';
import { SortPipe } from './common/pipes/sort.pipe';
import { DataTableComponent } from './common/components/data-table/data-table.component';
import { TableColumnComponent } from './common/components/table-column/table-column.component';
import { MessageBoxComponent } from './common/components/message-box/message-box.component';
import { DialogComponent } from './common/components/dialog/dialog.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    NumberRequiredDirective,
    SortPipe,
    DataTableComponent,
    TableColumnComponent,
    MessageBoxComponent,
    DialogComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
