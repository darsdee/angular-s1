export enum DIALOG_TYPES {
    ALERT = "alert",
    CONFIRM = "confirm",
    MAP_VIEW = "mapView"
}