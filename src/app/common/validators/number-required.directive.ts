import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[number-required][ngModel]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NumberRequiredDirective, multi: true }]
})
export class NumberRequiredDirective implements Validator {
  validate(c: AbstractControl): { [key: string]: any; } {
    if (isNaN(c.value)) {
      return { "nan": true };
    }
  }




}
