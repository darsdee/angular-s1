import { Component, Input } from '@angular/core';
import { DataTableComponent } from '../data-table/data-table.component';

@Component({
  selector: 'table-column',
  templateUrl: './table-column.component.html',
  styleUrls: ['./table-column.component.css']
})
export class TableColumnComponent {

  @Input() head: string;
  @Input() value: any

  constructor(private table: DataTableComponent) {
    this.table.addColumn(this);
  }

}
