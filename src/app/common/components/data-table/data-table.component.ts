import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent {

  @Input() data: any[];
  @Input() sortable: boolean;
  @Input() sortBy: string;
  @Input() sortByType: string
  @Output() editRow = new EventEmitter<number>();
  @Output() deleteRow = new EventEmitter<number>();
  columns: any[];

  addColumn(column: any): void {
    this.columns.push(column);
  }

  constructor() {
    this.columns = [];
  }

  edit(idx: number): void {
    this.editRow.emit(idx);
  }

  delete(idx: number): void {
    this.deleteRow.emit(idx);
  }

}
