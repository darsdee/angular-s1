import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  title: string;
  content: any;
  type: string;

  constructor(private dialog: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) data: any) {
    this.title = data.title;
    this.content = data.content;
    this.type = data.type;
  }

  close(flag: boolean): void {
    this.dialog.close(flag);
  }

}
