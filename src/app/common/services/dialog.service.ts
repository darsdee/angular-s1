import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material";

@Injectable({ providedIn: "root" })
export class DialogService {
  constructor(private dialog: MatDialog) { }

  openDialog(data: object, component: any): any {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = data;
    dialogConfig.disableClose = false;
    return this.dialog.open(component, dialogConfig);
  }
}
