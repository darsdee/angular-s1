import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {

  transform(list: any[], sortable: boolean, key: string, type: string): any[] {
    if (sortable) {
      if (list != undefined) {
        return list.sort((obj1, obj2) => {
          if (type === "number") {
            if (Number(obj1[key]) > Number(obj2[key])) {
              return 1;
            }
            if (Number(obj1[key]) < Number(obj2[key])) {
              return -1;
            }
            return 0;
          } else {
            return obj1[key].localeCompare(obj2[key]);
          }
        });
      }
    }
  }

}
